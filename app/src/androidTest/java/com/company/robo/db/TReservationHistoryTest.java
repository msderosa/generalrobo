package com.company.robo.db;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.IsolatedContext;
import android.test.mock.MockContentResolver;

public class TReservationHistoryTest extends AndroidTestCase {
    //private Database db;

  public void setUp() throws Exception {
    super.setUp();
    Context ctx = new IsolatedContext(new MockContentResolver(), this.getContext());
    this.setContext(ctx);
        // db = new Database(ctx, new Check(null));
  }

  public void testNothing() {
    assertTrue(true);
  }

//    public void testFindAll() {
//        final TReservationHistoryHistoryDao dao = new TReservationHistoryHistoryDao(db);
//        dao.findAll(Api.Io.Sync).subscribe(new Action1<List<ReservationHistory>>() {
//            @Override
//            public void call(List<ReservationHistory> hs) {
//                assertTrue("we should complete wo/ errors", hs.size() >= 0);
//            }
//        }, new Action1<Throwable>() {
//            @Override
//            public void call(Throwable e) {
//                fail(e.getMessage());
//            }
//        });
//    }
//
//    final String name = "Park Plus Airport Parking";
//    final String address = "99 Bessemer St";
//    final DateTime now = DateTime.now();
//
//    public void testInsertDelete() {
//        final ReservationHistory history = createHistory();
//
//        final TReservationHistoryHistoryDao dao = new TReservationHistoryHistoryDao(db);
//        final long id = dao.insert(history);
//        history.setId(id);
//
//        dao.findAll(Api.Io.Sync).subscribe(new Action1<List<ReservationHistory>>() {
//            @Override
//            public void call(List<ReservationHistory> hs) {
//                assertTrue("we should complete wo/ errors", hs.size() >= 1);
//                boolean founded = false;
//                for (ReservationHistory history : hs) {
//                    if (history.getId().equals(id)) {
//                        founded = true;
//                        break;
//                    }
//                }
//                assertTrue("reservation history with id " + id + " must be founded", founded);
//            }
//        }, new Action1<Throwable>() {
//            @Override
//            public void call(Throwable e) {
//                fail(e.getMessage());
//            }
//        });
//
//        final ReservationHistory merged = dao.findById(id);
//        assertNotNull(merged);
//        assertEquals(name, merged.getName());
//        assertEquals(address, merged.getAddress());
//        assertEquals(now.plusHours(1), merged.getArrival());
//        assertEquals(9d, merged.getPrice());
//        assertEquals(2d, merged.getServiceFee());
//        assertEquals(ValueAddedService.Apr, merged.getType());
//
//        int cnt = dao.delete(history);
//        assertEquals(1, cnt);
//    }
//
//    public void testPrune() throws Exception {
//        final TReservationHistoryHistoryDao dao = new TReservationHistoryHistoryDao(db);
//        final long[] allCounts = new long[3];
//        allCounts[0] = dao.count();
//
//        ReservationHistory historyOld = createHistory();
//        historyOld.setDeparture(DateTime.now().minusMonths(1));
//
//        long[] toDeleteIds = new long[2];
//        toDeleteIds[0] = dao.insert(historyOld);
//        toDeleteIds[1] = dao.insert(createHistory());
//
//        allCounts[1] = dao.count();
//        // should be added 2 histories, 1 with old departure date, 1 with new departure date
//        assertEquals(allCounts[0] + 2, allCounts[1]);
//
//        dao.prune();
//        allCounts[2] = dao.count();
//        // should be deleted 1 old history
//        assertEquals(allCounts[1] - 1, allCounts[2]);
//        ReservationHistory notDeleted = dao.findById(toDeleteIds[1]);
//        assertNotNull(notDeleted);
//        int count = dao.delete(notDeleted);
//        assertEquals(1, count);
//    }
//
//
//    private ReservationHistory createHistory() {
//        final ReservationHistory history = new ReservationHistory();
//        history.setName(name);
//        history.setAddress(address);
//        history.setArrival(now.plusHours(1));
//        history.setDeparture(now.plusHours(3));
//        history.setType(ValueAddedService.Apr);
//        history.setPrice(9d);
//        history.setServiceFee(2d);
//        history.setEmail("bensann@bestparking.com");
//        history.setReservationCode("BP123456");
//        return history;
//    }
}
