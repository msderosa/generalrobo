package com.company.robo;

import android.app.Application;
import com.company.robo.injection.AppComponent;
import com.company.robo.injection.AppModule;
import com.company.robo.injection.DaggerAppComponent;

public class App extends Application {
  private AppComponent component;

  @Override
  public void onCreate() {
    super.onCreate();

    component = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .build();
  }

  public AppComponent getComponent() { return component; }

}
