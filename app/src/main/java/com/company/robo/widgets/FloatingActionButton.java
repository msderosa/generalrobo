
/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.company.robo.widgets;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.company.robo.R;

public class FloatingActionButton extends FrameLayout implements Checkable {
  private int backgroundOn;
  private int backgroundOff;
  private boolean checked;
  private Drawable srcOn;
  private Drawable srcOff;

  public FloatingActionButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    initShadow();

    TypedArray as = context.obtainStyledAttributes(attrs, R.styleable.FloatingActionButton);
    initAttributes(as);
    addView(new FabImageView(getContext()));
  }

  public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initShadow();

    TypedArray as = context.obtainStyledAttributes(attrs, R.styleable.FloatingActionButton, defStyleAttr, 0);
    initAttributes(as);
    addView(new FabImageView(getContext()));
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    initShadow();

    TypedArray as = context.obtainStyledAttributes(attrs, R.styleable.FloatingActionButton, defStyleAttr, defStyleRes);
    initAttributes(as);
    addView(new FabImageView(getContext()));
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private void initShadow() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setOutlineProvider(new ViewOutlineProvider() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void getOutline(View view, Outline outline) {
          outline.setOval(0, 0, getWidth(), getHeight());
        }
      });
      setClipToOutline(true);
    }
  }

  private void initAttributes(TypedArray as) {
    try {
      backgroundOff = as.getColor(R.styleable.FloatingActionButton_backgroundOff, getResources().getColor(R.color.theme_accent_2));
      backgroundOn = as.getColor(R.styleable.FloatingActionButton_backgroundOn, getResources().getColor(R.color.theme_accent_1_light));
      checked = as.getBoolean(R.styleable.FloatingActionButton_checked, false);
      srcOn = as.getDrawable(R.styleable.FloatingActionButton_srcOn);
      srcOff = as.getDrawable(R.styleable.FloatingActionButton_srcOff);
    } finally {
      as.recycle();
    }
  }

  @Override
  public boolean isChecked() {
    Checkable img = (Checkable) this.getChildAt(0);
    return img.isChecked();
  }

  @Override
  public void setChecked(boolean checked) {
    Checkable img = (Checkable) this.getChildAt(0);
    img.setChecked(checked);
  }

  @Override
  public void toggle() {
    Checkable img = (Checkable) this.getChildAt(0);
    img.toggle();
  }


  private class FabImageView extends ImageView implements Checkable, View.OnClickListener {

    public FabImageView(Context ctx) {
      super(ctx);
      this.setClickable(true);
      this.setOnClickListener(this);

      initDrawables();
      initLayout();
    }

    private void initDrawables() {
      Drawable circle = getResources().getDrawable(R.drawable.shp_floating_action_button);
      this.setBackground(circle);

      Drawable src = checked ? srcOff : srcOn;
      this.setImageDrawable(src);
      this.setScaleType(ScaleType.CENTER);
      this.setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initLayout() {
      float scale = getResources().getDisplayMetrics().density;

      LayoutParams ps = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
              ViewGroup.LayoutParams.MATCH_PARENT,
              Gravity.CENTER);
      this.setLayoutParams(ps);

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        this.setElevation(16 * scale);
      }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void setBackground(Drawable bg) {
      ColorFilter fltr = getBackgroundColorFilter();
      bg.setColorFilter(fltr);

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        super.setBackground(bg);
      } else {
        this.setBackgroundDrawable(bg);
      }
    }

    private ColorFilter getBackgroundColorFilter() {
      ColorFilter fltr;
      if (checked) {
        fltr = new PorterDuffColorFilter(backgroundOn, PorterDuff.Mode.ADD);
      } else {
        fltr = new PorterDuffColorFilter(backgroundOff, PorterDuff.Mode.ADD);
      }
      return fltr;
    }

    @Override
    public void setChecked(boolean chked) {
      checked = chked;
      this.updateImage();
    }

    /* called after updating the clicked state */
    private void updateImage() {
      final Drawable src = checked ? srcOn : srcOff;

      AnimatorSet anim1 = (AnimatorSet) AnimatorInflater.loadAnimator(this.getContext(), R.animator.start_to_gone);
      anim1.setTarget(this);
      final AnimatorSet anim2 = (AnimatorSet) AnimatorInflater.loadAnimator(this.getContext(), R.animator.gone_to_finish);
      anim2.setTarget(this);
      anim1.addListener(new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
        }

        @Override
        public void onAnimationEnd(Animator animation) {
          getBackground().setColorFilter(getBackgroundColorFilter());
          setImageDrawable(src);
          anim2.start();
        }

        @Override
        public void onAnimationCancel(Animator animation) {
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
        }
      });
      anim1.start();

    }

    @Override
    public boolean isChecked() {
      return checked;
    }

    @Override
    public void toggle() {
      this.setChecked(!checked);
    }

    @Override
    public void onClick(View v) {
      checked = !checked;
      this.updateImage();
    }
  }
}
