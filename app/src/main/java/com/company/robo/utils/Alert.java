package com.company.robo.utils;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.company.robo.frg.AlertDlg;
import com.company.robo.utils.Check;

public class Alert {

  public static void show(Activity activity, DialogFragment alertDlg, String tag,
                          String title, String msg) {
    Check.expected(title != null, "a title must be provided");
    Check.expected(msg != null, "a message must be provided");

    FragmentManager manager = activity.getFragmentManager();
    Fragment frg = manager.findFragmentByTag(tag);
    if (frg != null) {
      if (frg instanceof DialogFragment) {
        final DialogFragment dialogFragment = (DialogFragment) frg;
        dialogFragment.dismiss();
      } else {
        FragmentTransaction tx = manager.beginTransaction();
        tx.remove(frg);
        tx.commit();
      }
    }

    Bundle bundle = alertDlg.getArguments();
    if (bundle == null) {
      bundle = new Bundle();
      bundle.putString(AlertDlg.ARG_TITLE, title);
      bundle.putString(AlertDlg.ARG_MESSAGE, msg);
      bundle.putBoolean(AlertDlg.ARG_FINISH_ACTIVITY, false);
      alertDlg.setArguments(bundle);
    } else {
      bundle.putString(AlertDlg.ARG_TITLE, title);
      bundle.putString(AlertDlg.ARG_MESSAGE, msg);
      bundle.putBoolean(AlertDlg.ARG_FINISH_ACTIVITY, false);
    }
    alertDlg.show(manager, tag);
	}
}
