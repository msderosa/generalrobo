package com.company.robo.utils.events;

public class StateEvent<T> {
  public final T previous;
  public final T current;
  public final String label;

  public StateEvent(T prev, T curr, String lbl) {
    this.previous = prev;
    this.current = curr;
    this.label = lbl;
  }
}
