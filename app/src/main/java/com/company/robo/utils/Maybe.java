package com.bstprkng.core.util;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class Maybe<T> implements Iterable<T>, Serializable {
	private static final long serialVersionUID = 4108367776258885L;

	public interface Visitor<T, S> {
        S onSome(T x);
        S onNothing();
    }
    
    public abstract <S> S accept(Visitor<T, S> visitor);

    private static abstract class ImmutableIterator<T> 
     implements Iterator<T> {
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    private static class Nothing<T> extends Maybe<T> {
		private static final long serialVersionUID = -5996870725320791135L;
		
		Iterator<T> EMPTY = new ImmutableIterator<T>() {
            public boolean hasNext() { return false; }

            public T next() {
                throw new NoSuchElementException();
            }
        };
        
        public Iterator<T> iterator() { return EMPTY; }

        @Override
        public <S> S accept(Maybe.Visitor<T, S> visitor) {
            return visitor.onNothing();
        }
    }
    
    private static Nothing<Object> nothing = new Nothing<Object>();
    
    private static class Just<T> extends Maybe<T> {
		private static final long serialVersionUID = 127411104260005490L;
		private T value;
        
        Just(T value) {
            this.value = value;
        }
        
        public boolean equals(Object o) {
            return o instanceof Just<?> &&
              (((Just<?>) o).value).equals(value);
        }

        public int hashCode() {
            return value.hashCode();
        }
        
        public Iterator<T> iterator()
        {
            return new ImmutableIterator<T>() {
                boolean hasNext = true;

                public boolean hasNext() {
                    return hasNext;
                }

                public T next()
                {
                    if (!hasNext) throw new NoSuchElementException();
                    hasNext = false;
                    return value;
                }
            };
        }

        @Override
        public <S> S accept(Maybe.Visitor<T, S> visitor)
        {
            return visitor.onSome(value);
        }
    }
    
    static <T> Maybe<T> some(Maybe<T> value) {
        return value == null ? Maybe.<T> nothing() : value;
    }

    @SuppressWarnings("unchecked")
    public static <T> Maybe<T> just(T value) {
        return value == null              ? Maybe.<T> nothing() : 
                value instanceof Maybe<?> ? (Maybe<T>) value    : new Just<T>(value);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> Maybe<T> nothing() {
    	Nothing<T> non = (Nothing<T>) nothing;
        return non;
    }
}