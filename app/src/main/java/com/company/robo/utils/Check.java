package com.company.robo.utils;

import android.os.Build;
import android.util.Log;
import com.company.robo.BuildConfig;
import com.company.robo.diagnostics.ITracker;
import javax.inject.Inject;

public class Check {
  private static final String TAG = Check.class.getSimpleName();
  private ITracker tracker;

  @Inject
  public Check(ITracker tracker) {
    this.tracker = tracker;
  }

  public void expect(boolean ok, String msg) {
    if (!ok) {
      if (BuildConfig.DEBUG) {
        Log.e(TAG, msg);
        throw new AssertionError(msg);
      } else {
        tracker.log(msg);
      }
    }
  }

  public void fail(String msg) {
    if (BuildConfig.DEBUG) {
      Log.e(TAG, msg);
      throw new AssertionError(msg);
    } else {
      tracker.log(msg);
    }
  }

  public static void expected(boolean ok, String msg) {
    if (BuildConfig.DEBUG && !ok) {
      Log.e(TAG, msg);
      throw new AssertionError(msg);
    }
  }

  public static void failed(String msg) {
    if (BuildConfig.DEBUG) {
      Log.e(TAG, msg);
      throw new AssertionError(msg);
    }
  }
}
