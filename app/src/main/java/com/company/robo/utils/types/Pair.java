package com.company.robo.utils.types;

public class Pair<S, T> {
  public S first;
  public T second;

  public Pair(S fst, T snd) {
    first = fst;
    second = snd;
  }

  @Override
  public String toString() {
		return "(" + first + ", " + second + ")";
	}

}
