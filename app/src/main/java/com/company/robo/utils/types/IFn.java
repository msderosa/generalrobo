package com.company.robo.utils.types;

public interface IFn<S, T> {

  T run(S s);
}
