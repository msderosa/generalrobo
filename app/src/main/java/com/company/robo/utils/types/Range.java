package com.company.robo.utils.types;

import com.company.robo.utils.Check;

public class Range<C extends Comparable<C>> {
  public final C lower;
  public final C upper;
  public final Boundary lowerBoundary;
  public final Boundary upperBoundary;

  public Range(C low, Boundary lowB, C up, Boundary upB) {
    lower = low;
    lowerBoundary = lowB;
    upper = up;
    upperBoundary = upB;
  }

  public static <C extends Comparable<C>> Range<C> open(C lower, C upper) {
    return new Range<C>(lower, Boundary.Open, upper, Boundary.Open);
  }

  public static <C extends Comparable<C>> Range<C> closed(C lower, C upper) {
    return new Range<C>(lower, Boundary.Closed, upper, Boundary.Closed);
  }

  public static <C extends Comparable<C>> Range<C> closedOpen(C lower, C upper) {
    return new Range<C>(lower, Boundary.Closed, upper, Boundary.Open);
  }

  public static <C extends Comparable<C>> Range<C> openClosed(C lower, C upper) {
    return new Range<C>(lower, Boundary.Open, upper, Boundary.Closed);
  }

  /**
   * Returns {@code true} if this range is of the form {@code [v..v)} or
   * {@code (v..v]}. (This does not encompass ranges of the form
   * {@code (v..v)}, because such ranges are <i>invalid</i> and can't be
   * constructed at all.)
   * <p/>
   * <p/>
   * Note that certain discrete ranges such as the integer range
   * {@code (3..4)} are <b>not</b> considered empty, even though they contain
   * no actual values. In these cases, it may be helpful to preprocess ranges
   */
  public boolean isEmpty() {
    return lower.equals(upper)
            && (this.lowerBoundary == Boundary.Open || this.upperBoundary == Boundary.Open);
  }

  /**
   * Returns {@code true} if {@code value} is within the bounds of this range.
   * For example, on the range {@code [0..2)}, {@code contains(1)} returns
   * {@code true}, while {@code contains(2)} returns {@code false}.
   */
  public boolean contains(C value) {
    Check.expected(value != null, "invalid parameter");
    boolean containedByLower;
    if (this.lowerBoundary == Boundary.Open) {
      containedByLower = (lower.compareTo(value) < 0);
    } else {
      containedByLower = (lower.compareTo(value) <= 0);
    }
    boolean containedByUpper;
    if (this.upperBoundary == Boundary.Open) {
      containedByUpper = (upper.compareTo(value) > 0);
    } else {
      containedByUpper = (upper.compareTo(value) >= 0);
    }
    return containedByLower && containedByUpper;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((lower == null) ? 0 : lower.hashCode());
    result = prime * result
            + ((lowerBoundary == null) ? 0 : lowerBoundary.hashCode());
    result = prime * result + ((upper == null) ? 0 : upper.hashCode());
    result = prime * result
            + ((upperBoundary == null) ? 0 : upperBoundary.hashCode());
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Range<C> other = (Range<C>) obj;
    if (lower == null) {
      if (other.lower != null)
        return false;
    } else if (!lower.equals(other.lower))
      return false;
    if (lowerBoundary != other.lowerBoundary)
      return false;
    if (upper == null) {
      if (other.upper != null)
        return false;
    } else if (!upper.equals(other.upper))
      return false;
    if (upperBoundary != other.upperBoundary)
      return false;
    return true;
  }

  private enum Boundary {
    Open, Closed
  }

}
