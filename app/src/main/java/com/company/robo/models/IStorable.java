package com.company.robo.models;

import android.content.SharedPreferences;
import android.os.Bundle;

public interface IStorable {
	
	void write(SharedPreferences prefs);
	Bundle toInstanceState();
	IStorable fromInstanceState(Bundle data);
}
