package com.company.robo.injection;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.company.robo.act.MainActivity;
import com.company.robo.diagnostics.ITracker;
import com.company.robo.diagnostics.NullTracker;
import com.company.robo.utils.Check;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

@Module
public class AppModule {
  private final Context context;

  public AppModule(Context ctx) { this.context = ctx; }

  @Provides
  Context providesApplicationContext() {
    return context;
  }

  @Provides @Singleton
  Bus provideBus() {
    return new Bus();
  }

  @Provides
  ITracker provideTracker() {
    return new NullTracker();
  }

  @Provides @Singleton
  RequestQueue provideRequestQueue() {
    return Volley.newRequestQueue(context);
  }

  @Provides
  Check provideCheck(ITracker tracker) {
    return new Check(tracker);
  }
}
