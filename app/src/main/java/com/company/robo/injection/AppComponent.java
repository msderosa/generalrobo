package com.company.robo.injection;

import com.android.volley.RequestQueue;
import com.company.robo.act.MainActivity;
import com.company.robo.diagnostics.ITracker;
import com.company.robo.svc.HttpJsonService;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(
        modules = AppModule.class
)
public interface AppComponent {

  // provision
  Bus getBus();
  ITracker getTracker();
  RequestQueue getRequestQueue();

  // gen:insert activities
  MainActivity inject(MainActivity ctx);

  // gen:insert fragments

  // gen:insert services
  HttpJsonService inject(HttpJsonService ctx);
}
