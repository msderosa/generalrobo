package com.company.robo.act;

import android.support.v7.app.ActionBarActivity;
import com.company.robo.App;
import com.company.robo.injection.AppComponent;

public class BaseActivity extends ActionBarActivity {

  public AppComponent getComponent() {
    App app = (App) this.getApplication();
    return app.getComponent();
  }

}
