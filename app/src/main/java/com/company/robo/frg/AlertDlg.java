package com.company.robo.frg;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import com.company.robo.utils.Check;

public class AlertDlg extends DialogFragment {
  public static final String ARG_TITLE = "title";
  public static final String ARG_MESSAGE = "message";
  public static final String ARG_FINISH_ACTIVITY = "finishActivity";
	
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    final Bundle arguments = this.getArguments();

    String title = arguments.getString(ARG_TITLE);
    String msg = arguments.getString(ARG_MESSAGE);
    final boolean finishActivity = arguments.getBoolean(ARG_FINISH_ACTIVITY);

    Check.expected(title != null && msg != null, "title and message args are required: " + title + ", " + msg);

    AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity())
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (finishActivity) {
                  AlertDlg.this.getActivity().finish();
                }
              }
            });
    return builder.create();
	}	

}
