package com.company.robo.diagnostics;

public interface ITracker {

  void trackEvent(String msg);

  void log(String msg);
}
