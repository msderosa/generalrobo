package com.company.robo.diagnostics;

public class NullTracker implements ITracker {

  @Override
  public void trackEvent(String msg) { }

  @Override
  public void log(String msg) { }
}
