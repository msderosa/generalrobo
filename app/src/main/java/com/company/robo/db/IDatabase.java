package com.company.robo.db;

import android.database.sqlite.SQLiteDatabase;

public interface IDatabase {

  SQLiteDatabase getWritableDatabase();
  SQLiteDatabase getReadableDatabase();
	
}
