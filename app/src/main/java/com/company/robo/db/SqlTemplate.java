package com.company.robo.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.company.robo.utils.types.IFn;

public class SqlTemplate<T> {
    //private static final String TAG = SqlTemplate.class.getSimpleName();
    private final IDatabase helper;

    public SqlTemplate(IDatabase hlper) {
        helper = hlper;
    }

    public T find(String sql, String[] args, IFn<Cursor, T> fn) {
        T result;
        SQLiteDatabase db;
        Cursor c = null;
        try {
            synchronized (helper) {
                db = helper.getReadableDatabase();
                c = db.rawQuery(sql, args);
                result = fn.run(c);
            }
        } catch (RuntimeException e) {
            if (c != null) {
                c.close();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return result;
    }

    public long insert(String table, T obj, IFn<T, ContentValues> fn) {
        SQLiteDatabase db;
        long total;
        synchronized (helper) {
            db = helper.getWritableDatabase();
            ContentValues vs = fn.run(obj);
            total = db.insertOrThrow(table, null, vs);
        }

        return total;
    }

    public int delete(String table, String where, String[] args) {
        SQLiteDatabase db;
        int total;
        synchronized (helper) {
            db = helper.getWritableDatabase();
            total = db.delete(table, where, args);
        }
        return total;
    }

    public int update(String table, String where, String[] args, T obj,
                      IFn<T, ContentValues> fn) {
        SQLiteDatabase db;
        int total;
        synchronized (helper) {
            db = helper.getWritableDatabase();
            ContentValues vs = fn.run(obj);
            total = db.update(table, vs, where, args);
        }

        return total;
    }

}
