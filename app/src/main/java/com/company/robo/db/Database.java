package com.company.robo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.company.robo.utils.Check;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.inject.Inject;

/*
 * For scripts make sure that where separators are needed that there is at least
 * a ' ' as carriage returns are not enough. For testing scripts cut and pastes 
 * via emacs add extra random characters so it is best to run scripts from the 
 * command line locally and then try loading to a database via Android code.
 */
public class Database extends SQLiteOpenHelper implements IDatabase {
  public static final String DATABASE_NAME = "main.db";
  public static final int DATABASE_VERSION = 12;
  private final Context ctx;
  private final Check check;

  @Inject
  public Database(Context context, Check ck) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    check = ck;
    ctx = context;
  }

  /*
	 * (/) -> database (version n)
	 */
  @Override
  public void onCreate(SQLiteDatabase db) {
//    db.execSQL(getResourceScript(ctx, R.raw.v0003_create_serviceareas));
//    db.execSQL(getResourceScript(ctx, R.raw.v0004_insert_serviceareas));
//    db.execSQL(getResourceScript(ctx, R.raw.v0005_update_serviceareas_nyc));
//    and dont forget to increment the version number
  }

  /*
	 * database (version n) -> datbase (version n + m)
	 */
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//    switch (oldVersion) {
//      case 1: db.execSQL(getResourceScript(ctx, R.raw.v0002_drop_orders));
//      case 2: db.execSQL(getResourceScript(ctx, R.raw.v0003_create_serviceareas));
//      case 3: db.execSQL(getResourceScript(ctx, R.raw.v0004_insert_serviceareas));
//    }
  }
	
  private String getResourceScript(Context ctx, int id) {
    StringBuilder bldr = new StringBuilder();
    InputStream stream = new BufferedInputStream(ctx.getResources().openRawResource(id));
    BufferedReader lines = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));
    try {
      String line = lines.readLine();
      while(line != null) {
        bldr.append(line);
        line = lines.readLine();
      }
      lines.close();
    } catch (IOException e) {
      check.fail(e.getMessage());
    }
    return bldr.toString();
  }

}
