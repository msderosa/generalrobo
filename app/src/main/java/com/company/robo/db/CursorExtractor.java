package com.company.robo.db;

import android.database.Cursor;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CursorExtractor {

  public enum Mode {
    STRICT,
    ZERO_IS_NULL
  }

  public static String extractString(Cursor c, String fld) {
    int idx = c.getColumnIndex(fld);
    return c.getString(idx);
  }

  public static boolean extractBoolean(Cursor c, String fld) {
    int idx = c.getColumnIndex(fld);
    short temp = c.getShort(idx);
    if (temp == 1) {
      return true;
    } else if (temp == 0) {
      return false;
    } else {
      throw new IllegalStateException("invalid field contents");
    }
  }

  public static Float extractFloat(Cursor c, String fld, Mode m) {
    int idx = c.getColumnIndex(fld);
    Float temp = c.getFloat(idx);
    if (temp == 0 && m == Mode.ZERO_IS_NULL) {
      temp = null;
    }
    return temp;
  }

  public static Double extractDouble(Cursor c, String fld, Mode m) {
    int idx = c.getColumnIndex(fld);
    Double temp = c.getDouble(idx);
    if (temp == 0 && m == Mode.ZERO_IS_NULL) {
      temp = null;
    }
    return temp;
  }

  /**
   * The cursor will return a null integer field as a 0. Since this leaves us
   * in an indeterminate situation its necessary to specify how to treat null
   * values manually.
   */
  public static Long extractLong(Cursor c, String fld, Mode m) {
    int idx = c.getColumnIndex(fld);
    long temp = c.getLong(idx);
    if (temp == 0 && m == Mode.ZERO_IS_NULL) {
      return null;
    } else {
      return temp;
    }
  }

  public static Date extractDate(Cursor c, String fld) {
    Long milli = extractLong(c, fld, Mode.ZERO_IS_NULL);
    if (milli == null) {
      return null;
    } else {
      Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
      cal.setTimeInMillis(milli);
      return cal.getTime();
    }
  }
}
