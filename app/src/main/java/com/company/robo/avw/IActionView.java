package com.company.robo.avw;

import android.app.Activity;

public interface IActionView<T> {

  void register(Activity ctx , T model, Object... others);

  void unregister();

}
