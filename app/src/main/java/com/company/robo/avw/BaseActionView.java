package com.company.robo.avw;

import android.app.Activity;
import android.view.View;

public abstract class BaseActionView<T> implements IActionView<T> {
  private Activity activity;
  private T model;

  public BaseActionView() { }

  public Activity getActivity() {
		return activity;
	}

  public BaseActionView<T> setActivity(Activity a) {
    this.activity = a;
    return this;
  }

	public T getModel() {
		return model;
	}

  public View findViewById(int id) {
		return activity.findViewById(id);
	}

  public void register(Activity ctx, T model) {
    this.activity = ctx;
    this.model = model;
  }

  public void unregister() {
    this.activity = null;
  }

}
