package com.company.robo.svc;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.company.robo.App;
import com.company.robo.utils.Annotations.RequiredPermissions;
import com.squareup.otto.Bus;

import org.json.JSONObject;

import javax.inject.Inject;

@RequiredPermissions("android.permission.INTERNET")
public class HttpJsonService extends Service {
  @Inject public Bus bus;
  @Inject public RequestQueue reqQueue;

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    ((App) getApplication()).getComponent().inject(this);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    reqQueue.stop();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {

    // We want this service to continue running until it is explicitly stopped, so return sticky.
    return START_STICKY;
  }

  public void doGetRequest(String url,
                          Response.Listener<JSONObject> rspListener,
                          Response.ErrorListener errListener) {
    JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
            url, null, rspListener, errListener);
    reqQueue.add(req);
  }

  public void doPostRequest(String url, JSONObject data, Response.Listener<JSONObject> rspListener,
                            Response.ErrorListener errListener) {
    JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
            url, data, rspListener, errListener);
    reqQueue.add(req);
  }

  // the onResponse callback runs in the main ui thread
  class RspListener implements Response.Listener<JSONObject> {

    public RspListener() {}

    @Override
    public void onResponse(JSONObject jsonObject) {

    }
  }

  // the onErrorResponse callback runs in the main ui thread
  class ErrListener implements Response.ErrorListener {

    public ErrListener() {}

    @Override
    public void onErrorResponse(VolleyError volleyError) {

    }
  }

}
