import robo.tasks.ActivityTask
import robo.tasks.FragmentTask
import robo.tasks.TableTask
import robo.tasks.ModelTask

if (args.size() == 0) {
    println "no generation command specified"
} else if (args[0] == "activities") {
    new ActivityTask().run()
} else if (args[0] == "fragments") {
    new FragmentTask().run()
} else if (args[0] == "tables") {
    new TableTask().run()
} else if (args[0] == "models") {
    new ModelTask().run()
} else {
    println "unable to find task for command $args"
}



