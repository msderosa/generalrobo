package robo.tasks

import robo.Export

class ActivityTask extends Task {

    def run() {
        def info = getPackageRootInfo()
        def spec = new File("tools/specs/activities.xml")
        def activities = new XmlSlurper().parse(spec)

        _mkActivityDir(info['packageRootDir'])
        _output(info, activities);
    }

    def _output(info, activities) {
        def exp = new Export()
        activities.activity.each({node ->

            def activityFile = "${info['packageRootDir']}/act/${node.name.text()}Activity.java"
            if (node.type.text() == "Default") {
                def templateFile = "tools/templates/act${node.type.text()}.tplt"
                def binding = [packageRoot: info['packageRoot'],
                               activity: node]
                exp.exportGPathResult(binding, activityFile, templateFile)


                def layoutFile = "${build_source_dir}/res/layout/act_${node.name.text().toLowerCase()}.xml"
                templateFile = "tools/templates/lay${node.type.text()}.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)
            } else if (node.type.text() == "Detail") {
                def templateFile = "tools/templates/actDefault.tplt"
                def binding = [packageRoot: info['packageRoot'],
                               activity: node]
                exp.exportGPathResult(binding, activityFile, templateFile)

                def layoutFile = "${build_source_dir}/res/layout/act_${node.name.text().toLowerCase()}.xml"
                templateFile = "tools/templates/lay${node.type.text()}.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)
            } else if (node.type.text() == "Drawer") {
                def templateFile = "tools/templates/actDrawer.tplt"
                def binding = [packageRoot: info['packageRoot'],
                               activity: node]
                exp.exportGPathResult(binding, activityFile, templateFile)

                def layoutFile = "${build_source_dir}/res/layout/act_${node.name.text().toLowerCase()}.xml"
                templateFile = "tools/templates/lay${node.type.text()}.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)

                layoutFile = "${build_source_dir}/res/layout/navdrawer.xml"
                templateFile = "tools/templates/lay${node.type.text()}Navdrawer.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)

                layoutFile = "${build_source_dir}/res/layout/navdrawer_item.xml"
                templateFile = "tools/templates/lay${node.type.text()}NavdrawerItem.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)

            } else {
                throw Exception("bad activity type")
            }
        })
    }

    private void _mkActivityDir(String baseCodeDir) {
        String temp = baseCodeDir + '/act'
        File hdl = new File(temp);
        if (!hdl.exists()) {
            hdl.mkdir()
        }
    }

}
