package robo.tasks

import robo.Export

class TableTask extends Task {

    def run() {
        def info = getPackageRootInfo()
        def spec = new File("tools/specs/tables.xml")
        def tables = new XmlSlurper().parse(spec)

        _mkDbDirs(info['packageRootDir'])
        _output(info, tables);

    }

    def _output(info, tables) {
        def exp = new Export()
        tables.table.each({table ->

            def interfaceFile = "${info['packageRootDir']}/db/I${table.name.text()}.java"
            def templateFile = "tools/templates/tableInterface.tplt"
            def binding = [packageRoot: info['packageRoot'],
                           table: table]
            exp.exportGPathResult(binding, interfaceFile, templateFile)

            def modelFile = "${info['packageRootDir']}/db/data/${table.nameSingular.text()}.java"
            templateFile = "tools/templates/tableModel.tplt"
            exp.exportGPathResult(binding, modelFile, templateFile)

            def implFile = "${info['packageRootDir']}/db/T${table.name.text()}.java"
            templateFile = "tools/templates/tableImplementation.tplt"
            exp.exportGPathResult(binding, implFile, templateFile)

            def sqlFile = "${build_source_dir}/res/raw/v${table.seq.text()}_create_${table.name.text().toLowerCase()}.sql"
            templateFile = "tools/templates/tableSql.tplt"
            exp.exportGPathResult(binding, sqlFile, templateFile)
        })
    }

    private void _mkDbDirs(String baseCodeDir) {
        [baseCodeDir + '/db/data', "${build_source_dir}/res/raw"].each{temp ->
            File hdl = new File(temp);
            if (!hdl.exists()) {
                hdl.mkdir()
            }
        }
    }

}