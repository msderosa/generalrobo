package robo.tasks

class Task {
    def build_source_dir = "app/src/main"

    def getPackageRootInfo() {
        def manifest = new File(build_source_dir + "/AndroidManifest.xml");
        def root = new XmlSlurper().parse(manifest)

        def packageRoot = root.@package.text()
        def baseCodeDir = packageRoot.replace('.', '/');
        def outputDir = build_source_dir + "/java/" + baseCodeDir;
        return [packageRoot: packageRoot, packageRootDir: outputDir];
    }
}