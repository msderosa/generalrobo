package robo.tasks

import robo.Export

class ModelTask extends Task {

    def run() {
        def info = getPackageRootInfo()
        def spec = new File("tools/specs/models.xml")
        def models = new XmlSlurper().parse(spec)

        _output(info, models);
    }

    def _output(info, models) {
        def exp = new Export()
        models.model.each({node ->

            def activityFile = "${info['packageRootDir']}/models/${node.name.text()}.java"
            def templateFile = "tools/templates/model.tplt"
            def binding = [packageRoot: info['packageRoot'],
                           model: node]
            exp.exportGPathResult(binding, activityFile, templateFile)
        })
    }

    private void _mkActivityDir(String baseCodeDir) {
        String temp = baseCodeDir + '/act'
        File hdl = new File(temp);
        if (!hdl.exists()) {
            hdl.mkdir()
        }
    }

}
