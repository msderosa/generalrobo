package robo.tasks

import robo.Export;

class FragmentTask extends Task {

    def run() {
        def info = getPackageRootInfo()
        def spec = new File("tools/specs/fragments.xml")
        def fragments = new XmlSlurper().parse(spec)

        _mkFragmentDir(info['packageRootDir'])
        _output(info, fragments);

    }

    def _output(info, fragments) {
        def exp = new Export()
        fragments.fragment.each({node ->
            def type = node.type.text()
            if (type == "ScheduleList") {
                def fragmentFile = "${info['packageRootDir']}/frg/${node.name.text()}Frg.java"
                def templateFile = "tools/templates/frg${node.type.text()}.tplt"
                def binding = [packageRoot: info['packageRoot'],
                               fragment: node]
                exp.exportGPathResult(binding, fragmentFile, templateFile)

                def adaptorFile = "${info['packageRootDir']}/frg/${node.name.text()}Adapter.java"
                templateFile = "tools/templates/frg${node.type.text()}Adapter.tplt"
                exp.exportGPathResult(binding, adaptorFile, templateFile)


                def layoutFile = "${build_source_dir}/res/layout/frg_${node.name.text().toLowerCase()}.xml"
                templateFile = "tools/templates/lay${node.type.text()}.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)

                layoutFile = "${build_source_dir}/res/layout/lsti_${node.name.text().toLowerCase()}.xml"
                templateFile = "tools/templates/lay${node.type.text()}Item.tplt"
                exp.exportGPathResult(binding, layoutFile, templateFile)
            }
        })
    }

    private void _mkFragmentDir(String baseCodeDir) {
        String temp = baseCodeDir + '/frg'
        File hdl = new File(temp);
        if (!hdl.exists()) {
            hdl.mkdir()
        }
    }

}