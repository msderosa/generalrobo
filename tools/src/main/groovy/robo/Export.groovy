package robo

import groovy.util.slurpersupport.GPathResult
import groovy.text.GStringTemplateEngine

class Export {

    void exportGPathResult(Map binding, String targetPath, String templatePath) {
        def hdl = new File(targetPath)
        if (!hdl.exists()) {
            def engine = new GStringTemplateEngine();
            def tplHdl = new File(templatePath)
            def template = engine.createTemplate(tplHdl).make(binding)
            hdl << template.toString();
        }
    }

}